--------------------------------------------
--  Welcome to my one file neovim config  --
--------------------------------------------
---     I try to make my stuff fast      ---
---    by doing some lazy loading and    ---
---        using minimalist plugins      ---
---                                      ---
---     This config is aimed to be:      ---
---   simple, logic and well documented  ---
---                                      ---
--- feel free to get steal stuff from it ---
--------------------------------------------

-----------------------------------------
--* Needed at the top of config stuff *--
-----------------------------------------

--* Remove useless warnings in lua_ls (the Lua LSP) *--
---@diagnostic disable: inject-field, param-type-mismatch, undefined-global, undefined-field, missing-fields, assign-type-mismatch, unbalanced-assignments, undefined-doc-name

--* Top variables and functions *--
local api = vim.api
local autocmd = api.nvim_create_autocmd
local augroup = api.nvim_create_augroup
local opt = vim.opt
local o = vim.o
local g = vim.g
local fn = vim.fn

local kmap = function(modes, map, cmd, desc)
	vim.keymap.set(modes, map, cmd, { desc = desc })
end

local cmd_kmap = function(map, cmd, desc)
	kmap("n", map, "<cmd>" .. cmd .. "<CR>", desc)
end

vim.filetype.add({ pattern = { [".*/hypr/.*%.conf"] = "hyprlang" } }) --* Enable hypr*.conf highlights

--* Custom settings *--
local json_path = fn.stdpath("config") .. "/settings.json"
local json_file = io.open(json_path, "r")
local content = json_file:read("*a")
json_file:close()
local settings = fn.json_decode(content)

--* Autocmds *--
autocmd("VimResized", {
	desc = "Automatically resize splits, when terminal window is moved",
	command = "wincmd =",
})

autocmd("TextYankPost", {
	desc = "highlights yanked text",
	callback = function()
		vim.highlight.on_yank({
			higroup = "IncSearch",
			timeout = 40,
		})
	end,
})

autocmd({ "CursorMoved", "CursorMovedI", "WinScrolled" }, {
	desc = "Fix scrolloff when you are at the EOF",
	group = augroup("ScrollEOF", { clear = true }),
	callback = function()
		if api.nvim_win_get_config(0).relative ~= "" then
			return -- Ignore floating windows
		end

		local win_height = fn.winheight(0)
		local scrolloff = math.min(o.scrolloff, math.floor(win_height / 2))
		local visual_distance_to_eof = win_height - fn.winline()

		if visual_distance_to_eof < scrolloff then
			local win_view = fn.winsaveview()
			fn.winrestview({ topline = win_view.topline + scrolloff - visual_distance_to_eof })
		end
	end,
})

autocmd("FileType", {
	desc = "Automatically Split help Buffers to the right",
	pattern = "help",
	command = "wincmd L",
})

autocmd("BufWritePre", {
	desc = "Autocreate a dir when saving a file",
	group = augroup("auto_create_dir", { clear = true }),
	callback = function(event)
		if event.match:match("^%w%w+:[\\/][\\/]") then
			return
		end
		local file = vim.uv.fs_realpath(event.match) or event.match
		fn.mkdir(fn.fnamemodify(file, ":p:h"), "p")
	end,
})

autocmd({ "UIEnter", "ColorScheme" }, {
	desc = "Corrects terminal background color according to colorscheme, see: https://www.reddit.com/r/neovim/comments/1ehidxy/you_can_remove_padding_around_neovim_instance/",
	callback = function()
		if api.nvim_get_hl(0, { name = "Normal" }).bg then
			io.write(string.format("\027]11;#%06x\027\\", api.nvim_get_hl(0, { name = "Normal" }).bg))
		end
		autocmd("UILeave", {
			callback = function()
				io.write("\027]111\027\\")
			end,
		})
	end,
})

autocmd("TermOpen", {
	desc = "Remove UI clutter in the terminal",
	callback = function()
		local is_terminal = api.nvim_get_option_value("buftype", { buf = 0 }) == "terminal"
		o.number = not is_terminal
		o.relativenumber = not is_terminal
		o.signcolumn = is_terminal and "no" or "yes"
	end,
})

autocmd("BufReadPost", {
	desc = "Auto jump to last position",
	group = augroup("auto-last-position", { clear = true }),
	callback = function(args)
		local position = api.nvim_buf_get_mark(args.buf, [["]])
		local winid = fn.bufwinid(args.buf)
		pcall(api.nvim_win_set_cursor, winid, position)
	end,
})

autocmd("BufWinEnter", {
	desc = "auto change local current directory",
	group = augroup("auto-project-root", {}),
	callback = function(args)
		if api.nvim_get_option_value("buftype", { buf = args.buf }) ~= "" then
			return
		end

		local root = vim.fs.root(args.buf, function(name, path)
			local pattern = { ".git", "Cargo.toml", "go.mod" }
			local multipattern = { "build/compile_commands.json" }
			local abspath = { fn.stdpath("config") }
			local parentpath = { "~/.config", "~/prj" }

			return vim.iter(pattern):any(function(filepat)
				return filepat == name
			end) or vim.iter(multipattern):any(function(filepats)
				return vim.uv.fs_stat(vim.fs.joinpath(path, vim.fs.normalize(filepats)))
			end) or vim.iter(abspath):any(function(dirpath)
				return vim.fs.normalize(dirpath) == path
			end) or vim.iter(parentpath):any(function(ppath)
				return vim.fs.normalize(ppath) == vim.fs.dirname(path)
			end)
		end)
		if root then
			vim.cmd.lcd(root)
		end
	end,
})

--* Bootstrap/Run lazy.nvim *--
local lazypath = fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	local out = fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
	if vim.v.shell_error ~= 0 then
		api.nvim_echo({
			{ "Failed to clone lazy.nvim:\n", "ErrorMsg" },
			{ out, "WarningMsg" },
			{ "\nPress any key to exit..." },
		}, true, {})
		fn.getchar()
		os.exit(1)
	end
end
opt.rtp:prepend(lazypath)

-----------------------
--* Neovim settings *--
-----------------------

--* Truly disable statusline (even in splits) *--
o.cmdheight = 0 -- Disable cmdline
o.laststatus = 0 -- The last window won't have a statusline
api.nvim_set_hl(0, "StatusLine", { link = "Normal" }) -- use background colors for statusline
api.nvim_set_hl(0, "StatusLineNC", { link = "Normal" })
o.statusline = '%{repeat("─",winwidth("."))}' -- for splits use the "-" character as a separator in the "statusline"
o.spell = false -- spellcheck
o.spelllang = settings.languages -- Spellcheck languages (defaults to en_us)
o.smoothscroll = true -- Makes scrolling smoother with wrapped lines
o.shiftround = true -- Round indent
o.winminwidth = 8 -- Minimum window width
o.autoread = true -- Disable annoying prompt when editing the same file on multiple neovim instaces
o.undofile = true -- Enable undofile, keeps an history of undos for a long time
o.number = true -- Show current line number
o.relativenumber = true -- Show relative line number
o.scrolloff = 10 -- Always keep 10 line between the edges of the screen when scrolling Verticaly
o.sidescrolloff = 10 -- Always keep 10 line between the edges of the screen when scrolling Horizontaly
o.shiftwidth = 2 -- Change number of space characters inserted for indentation
o.expandtab = true -- Use space instead of tabs
o.tabstop = 2 -- Tab characters are 2 spaces
o.ignorecase = true -- ignore casing of both lowercase & uppercase characters (with ignorecase, without smartcase)
o.smartcase = true -- Ignore casing for lowercase characters (with ignorecase & smartcase)
o.autoindent = true -- Copy indent from current line when starting a new line
o.smartindent = true -- Is like 'autoindent' but also recognizes some C syntax to change indent where appropriate.
o.breakindent = true -- Indent line wrap
o.wrap = false -- Disable line wrap (i have a keymap to enable it)
o.linebreak = true -- Do not cut words when wrapping
o.smarttab = true -- Make tabbing smarter
o.backup = false -- Disable backup
o.swapfile = false -- Disable swapfile
o.showtabline = 0 -- enable tabs (i enable tabs when i create a new tab)
o.mouse = "" -- Disable mouse
o.termguicolors = true -- Enable 24-bit RGB colors ( needed for alot of plugins )
o.cursorline = true -- Highlight cursorline
o.timeoutlen = 500 -- Time in milliseconds for a mapped sequence (<leader> + ?)
o.updatetime = 250 -- Controls how often the editor checks for file changes and updates its internal state
o.inccommand = "nosplit" -- Preview substitutions live, as you type
o.list = true -- Show invisible characters
opt.listchars = { tab = "▎ ", trail = "·", nbsp = "␣" } -- Type of invisible characters
o.signcolumn = "yes" -- Show sign column by default
g.mapleader = " " -- Set <leader> to <space>
g.maplocalleader = "," -- Set <localleader> to ;
o.shell = "/bin/zsh" -- Change default terminal shell
o.splitbelow = true -- Horizontal splits will spawn below by default
o.splitright = true -- Vertical splits will spawn to the right by default
o.diffopt = "internal,filler,closeoff,indent-heuristic,linematch:60,algorithm:histogram" -- Improves diff
--* Disable unused programming languages providers *--
g.loaded_perl_provider = 0
g.loaded_ruby_provider = 0

--* Changes cursor to "|" in Normal mode and to "_" in Insert mode *--
o.guicursor = "v-c-sm:block,n-i-ci-ve:ver25,r-cr-o:hor20,i-ci:hor30-iCursor,c-ci-cr:hor20-Cursor/lCursor"

-------------------------
--* Lazy.nvim plugins *--
-------------------------
require("lazy").setup({

	-- { --* frecency.nvim *--
	-- 	dir = "~/Projects/frecency.nvim/", -- For local development
	-- 	event = "VeryLazy",
	-- 	dependencies = {
	-- 		"ibhagwan/fzf-lua",
	-- 		"kkharji/sqlite.lua", -- Recomended
	-- 	},
	-- 	opts = { picker = "fzf-lua" },
	-- },

	{ --* Good looking colorscheme *--
		"folke/tokyonight.nvim",
		priority = 999, -- start before every plugin but after snacks.nvim
		config = function()
			vim.cmd.colorscheme("tokyonight-moon")
		end,
	},

	{ --* Good looking too *--
		"catppuccin/nvim",
		name = "catppuccin",
		event = "VeryLazy",
		priority = 999,
		opts = {
			compile_path = vim.fn.stdpath("cache") .. "/catppuccin",
			flavour = "auto", -- latte, frappe, macchiato, mocha
			dim_inactive = {
				enabled = true, -- dims the background color of inactive window
				shade = "dark",
				percentage = 0.15, -- percentage of the shade to apply to the inactive window
			},
			integrations = {
				cmp = false,
				gitsigns = false,
				treesitter = true,
				dadbod_ui = true,
				notify = false,
				mini = { enabled = true },
				alpha = false,
				blink_cmp = true,
				dashboard = false,
				flash = false,
				grug_far = true,
				indent_blankline = { enabled = false },
				mason = true,
				neotree = false,
				neogit = false,
				noice = true,
				nvimtree = false,
				treesitter_context = false,
				ufo = false,
				rainbow_delimiters = false,
				render_markdown = false,
				snacks = true,
				telescope = { enabled = false },
				which_key = true,
			},
		},
	},

	{ --* TS + LSP setup with cmp, mason, lspconfig, nvim-snippets *--
		"neovim/nvim-lspconfig",
		event = { "BufReadPre", "BufNewFile" },
		cmd = "LSPInfo",
		dependencies = {
			{ --* General grammar / syntax highlighting dependencies *--
				"nvim-treesitter/nvim-treesitter",
				build = ":TSUpdate",
				main = "nvim-treesitter.configs",
				opts = {
					sync_install = false,
					auto_install = true, -- Auto install highlighter if you enter a filetype which you don't have a paerser installed for it
					ignore_install = {}, -- Install every parser
					matchup = { enable = true, enable_quotes = true }, -- Needed for vim matchup
					indent = { enable = true }, -- Indentation based on treesitter for the = operator
					highlight = { enable = true }, -- Enable highlighting (otherwise there's none)
					ensure_installed = {
						"json", -- Language based parsers
						"javascript",
						"asm",
						"dockerfile",
						"diff",
						"desktop",
						"yaml",
						"html",
						"css",
						"markdown",
						"markdown_inline",
						"bash",
						"lua",
						"toml",
						"vimdoc",
						"csv",
						"go",
						"gomod",
						"nasm",
						"c",
						"cpp",
						"python",
						"make",
						"just",
						"ini",
						"ssh_config",
						"git_config",
						"gitcommit",
						"rust",
						"comment", -- Highlights comments
						"regex", -- Highlight Regex in the commandline
						"vim", -- Highlight vim commands in the commandline & files
						"gitcommit", -- Highlighting when commiting
						"hyprlang", -- Highlight Hyprland based config file
					},
					incremental_selection = { -- Use <C-Space> to incrementally select text
						enable = true,
						keymaps = {
							init_selection = "<C-space>",
							node_incremental = "<C-space>",
							scope_incremental = false,
							node_decremental = "<bs>",
						},
					},
				},
			},

			{ -- Ensures installation of LSPs / formatters / linters and DAPs
				"WhoIsSethDaniel/mason-tool-installer.nvim",
				dependencies = {
					{ --* Neovim third party utility pkg manager and loader *--
						"KingMichaelPark/mason.nvim",
						branch = "feat/add-uv-as-pypi-source",
						opts = {
							max_concurrent_installers = 16, -- Installer more tools at the same time
							ui = { border = "rounded" }, -- Rounded borders
							pip = { use_uv = true }, -- Use the faster uv pkg manager
						},
					},
				},
				config = function()
					local servers = {
						--* LSP -> Language Server Protocol *--
						"asm_lsp", -- Asm LSP
						"lua_ls", -- Lua LSP (the default formatter sucks, stylua is way better)
						"html", -- HTML LSP + formatter
						"cssls", -- Css LSP + formatter
						"ts_ls", -- JS + TS LSP
						"rust_analyzer", -- Rust LSP + formatter
						"bashls", -- Bash LSP
						"jedi_language_server", -- Python LSP
						"basedpyright", -- Python linter (i only use it for inlay hints)
						"docker_compose_language_service", -- Docker compose LSP
						"dockerls", -- DockerFile LSP
						"vimls", -- Vimscript LSP
						"gopls", -- Golang LSP
						"sqlls", -- SQL LSP

						--* Formatters *--
						"stylua", -- Lua formatter
						"mdformat", -- Markdown formatter
						"beautysh", -- Sh / Bash / Zsh / Fish formatter
						"taplo", -- Toml formatter (is an LSP under the hood)
						"biome", -- JS + TS + json incredibly fast formatter
						"ruff", -- Python fast formatter
						"clang-format", -- C & C++ formatter
						"gofumpt", -- Go formatter

						--* DAP -> Debug Adapter Protocol *--
						"cpptools", -- C/C++ Debugger using gdb
						"debugpy", -- Python Debugger
					}

					if settings.MuslLibc then -- MuslLibc is not compatible with some tools
						require("lspconfig").clangd.setup({})
						require("lspconfig").texlab.setup({})
					else
						table.insert(servers, "clangd") -- C++ Lsp & Formatter
						table.insert(servers, "texlab") -- Latex Lsp
					end

					require("mason-tool-installer").setup({
						ensure_installed = servers,
					})
				end,
			},

			"KingMichaelPark/mason-lspconfig.nvim", --* Auto-Load Mason installed LSPs to Neovim (and nvim-cmp) *--
		},

		config = function()
			--* Lsp keymaps *--
			cmd_kmap("gd", "lua vim.lsp.buf.definition()", "Goto definition")
			cmd_kmap("gD", "lua vim.lsp.buf.declaration()", "Goto implementation")
			cmd_kmap("gi", "lua vim.lsp.buf.implementation()", "Goto implementation")
			cmd_kmap("go", "lua vim.lsp.buf.type_definition()", "Goto type definition")
			cmd_kmap("gr", "lua vim.lsp.buf.references()", "See references in a quickfix list")
			cmd_kmap("gs", "lua vim.lsp.buf.signature_help()", "Signature help")
			cmd_kmap("<leader>r", "lua vim.lsp.buf.rename()", "Rename lsp object")

			require("lspconfig.ui.windows").default_options.border = "rounded" -- Makes lspconfig's windows rounded

			require("mason-lspconfig").setup()
			require("mason-lspconfig").setup_handlers({
				function(server_name)
					if settings.Blink then
						require("lspconfig")[server_name].setup({
							capabilities = require("blink.cmp").get_lsp_capabilities(),
						})
					else
						require("lspconfig")[server_name].setup({})
					end
				end,

				["asm_lsp"] = function()
					require("lspconfig").asm_lsp.setup({
						handlers = { -- Remove diagnostics, because there's too much
							["textDocument/publishDiagnostics"] = function() end,
						},
					})
				end,

				["basedpyright"] = function()
					require("lspconfig").basedpyright.setup({
						handlers = { -- Remove diagnostics, because there's too much
							["textDocument/publishDiagnostics"] = function() end,
						},
					})
				end,
			})

			vim.diagnostic.config({
				signs = {
					text = { -- Better icons
						[vim.diagnostic.severity.ERROR] = "✘",
						[vim.diagnostic.severity.WARN] = "",
						[vim.diagnostic.severity.HINT] = "⚑",
						[vim.diagnostic.severity.INFO] = "»",
					},
				},
				virtual_text = false, -- I use diagflow.nvim for diagnostics or -> { prefix = ' ● ' }, -- Change text preceding diagnostics
				underline = false, -- Disable underline for diagnostics
				update_in_insert = true, -- Show diagnostics even in Insert mode
			})
		end,
	},

	{ --* Completion *--
		-- My own nvim-cmp fork, it's a faster, bugless, featureful completion engine
		-- "iguanacucumber/magazine.nvim",
		--   version = "*",
		dir = "~/Projects/mag/magazine.nvim/", -- For local development
		name = "nvim-cmp", -- Otherwise highlighting gets messed up
		enabled = not settings.Blink,
		event = { "InsertEnter", "CmdlineEnter" },
		dependencies = {
			--* cmp sources *--
			{ dir = "~/Projects/mag/mag-nvim-lsp", name = "cmp-nvim-lsp" }, --* neovim's built-in LSP *--
			{ dir = "~/Projects/mag/mag-buffer", name = "cmp-buffer" }, --* Text within the buffer *--
			{ dir = "~/Projects/mag/mag-cmdline", name = "cmp-cmdline" }, --* Commands and their arg *--
			{ dir = "~/Projects/mag/mag-nvim-lua", name = "cmp-nvim-lua" }, --* Nvim lua api *--
			"https://codeberg.org/FelipeLema/cmp-async-path", --* Async path *--

			"echasnovski/mini.icons", --* icon support *--
			"rafamadriz/friendly-snippets", --* Boilerplate snippets *--

			{ "windwp/nvim-autopairs", opts = true }, --* Autopairs integrated with cmp *--

			{ --* Create custom snippets *--
				"chrisgrieser/nvim-scissors", -- Snippet maker
				dependencies = { "garymjr/nvim-snippets", opts = { friendly_snippets = true } }, -- Snippet engine (native snippets)
				opts = { snippetDir = fn.stdpath("config") .. "/snippets" },
			},
		},

		config = function()
			--* CMP setup *--
			local cmp = require("cmp")
			local cmdline = cmp.setup.cmdline
			local cmap = cmp.mapping
			local cmdmap = cmp.mapping.preset.cmdline

			cmp.event:on("confirm_done", require("nvim-autopairs.completion.cmp")) -- Autopairs integration

			cmdline({ "/", "?" }, { -- sources for search
				mapping = cmdmap(),
				sources = {
					{ name = "buffer", priority = 8 }, -- Text within current buffer
				},
			})

			cmdline({ ":" }, { -- sources for cmdline
				mapping = cmdmap(),
				sources = {
					{ name = "cmdline", priority = 10 }, -- Available commands
					{ name = "buffer", priority = 9 }, -- Text within current buffer
				},
			})

			cmdline({ ":lua" }, { -- sources for lua cmdline
				mapping = cmdmap(),
				sources = {
					{ name = "lazydev", group_index = 0, priority = 10 }, -- Improved lua_ls
					{ name = "nvim_lua", priority = 9 }, -- Nvim lua api
					{ name = "buffer", priority = 8 }, -- Text within current buffer
				},
			})

			cmdline({ ":h" }, { -- sources for help cmdline
				mapping = cmdmap(),
				sources = {
					{ name = "cmdline", priority = 10 }, -- Nvim lua api
					{ name = "buffer", priority = 1 }, -- Text within current buffer
				},
			})

			cmp.setup({ -- insert mode sources
				sources = {
					{ name = "snippets", priority = 11 }, -- Custom snippets
					{ name = "lazydev", group_index = 0, priority = 10 }, -- Improved lua_ls
					{ name = "nvim_lua", priority = 9 }, -- Nvim lua api
					{ name = "nvim_lsp", priority = 8 }, -- LSP
					{ name = "dap", priority = 8 }, -- Extra debugger info
					{ name = "buffer", priority = 7 }, -- Text within current buffer
					{ name = "async_path", priority = 7 }, -- Path
				},

				mapping = {
					-- Allow arrows to move cursor outside of selection
					["<Down>"] = cmap(function(fallback)
						cmp.close()
						fallback()
					end, { "i" }),

					["<Up>"] = cmap(function(fallback)
						cmp.close()
						fallback()
					end, { "i" }),

					["<C-e>"] = cmap.abort(),
					["<C-d>"] = cmap.scroll_docs(4),
					["<C-u>"] = cmap.scroll_docs(-4),

					-- Set enter to complete and tab to select
					["<CR>"] = cmap.confirm({ select = true }),
					["<C-Space>"] = cmap.complete(),
					["<Tab>"] = cmap.select_next_item(),
					["<S-Tab>"] = cmap.select_prev_item(),
				},

				window = { -- fancier windows
					completion = {
						border = {
							{ "󱐋", "WarningMsg" },
							{ "─", "Comment" },
							{ "╮", "Comment" },
							{ "│", "Comment" },
							{ "╯", "Comment" },
							{ "─", "Comment" },
							{ "╰", "Comment" },
							{ "│", "Comment" },
						},
					},
					documentation = {
						border = {
							{ "󰙎", "DiagnosticHint" },
							{ "─", "Comment" },
							{ "╮", "Comment" },
							{ "│", "Comment" },
							{ "╯", "Comment" },
							{ "─", "Comment" },
							{ "╰", "Comment" },
							{ "│", "Comment" },
						},
					},
				},

				performance = { -- Trying to improve performance
					debounce = 6,
					throttle = 10,
					fetching_timeout = 200,
					confirm_resolve_timeout = 60,
					async_budget = 1,
					max_view_entries = 200,
				},

				view = { entries = { vertical_positioning = "above" } }, -- Completion menu is above cursor
				experimental = { ghost_text = false }, -- No previewing text
				formatting = {
					fields = { "abbr", "kind" }, -- Name | icon
					format = function(_, item) -- lspkind behaviour with mini.icons and nvim-highlight-colors (add cute little icons and css colors)
						local color_item = require("nvim-highlight-colors").format(entry, { kind = item.kind })
						item.kind, item.kind_hl_group = require("mini.icons").get("lsp", item.kind) .. " "
						if color_item.abbr_hl_group then
							item.kind_hl_group = color_item.abbr_hl_group
							item.kind = color_item.abbr
						end
						return item
					end,
				},
			})
		end,
	},

	{ --* Create custom snippets *--
		"chrisgrieser/nvim-scissors", -- Snippet maker
		cmd = { "ScissorsAddNewSnippet", "ScissorsEditSnippet" },
		opts = { snippetDir = fn.stdpath("config") .. "/snippets" },
	},

	{ --* Fast, modern completion engine *--
		"saghen/blink.cmp",
		enabled = settings.Blink,
		build = "cargo build --release",
		dependencies = { --* Sources *--
			{ "saghen/blink.compat", opts = true }, --* Compatibility layer for nvim-cmp/magazine.nvim sources *--
			"mikavilpas/blink-ripgrep.nvim", --* Like the buffer source, but search for the whole project (w. ripgrep/rg) *--
			{ --* .env handling *--
				"philosofonusus/ecolog.nvim",
				opts = { integrations = { nvim_cmp = false, blink_cmp = true } },
			},
			"rafamadriz/friendly-snippets", --* Boilerplate snippets *--

			{ "windwp/nvim-autopairs", opts = true }, --* Autopairs integrated with cmp *--
			{ "xzbdmw/colorful-menu.nvim", opts = true }, --* Pretty menu *--

			-- { --* Autopairs "'[{( ... *--
			-- 	"echasnovski/mini.pairs",
			-- 	opts = {
			-- 		modes = { insert = true, command = true, terminal = false },
			-- 		-- skip autopair when next character is one of these
			-- 		skip_next = [=[[%w%%%'%[%"%.%`%$]]=],
			-- 		-- skip autopair when the cursor is inside these treesitter nodes
			-- 		skip_ts = { "string" },
			-- 		-- skip autopair when next character is closing pair
			-- 		-- and there are more closing pairs than opening pairs
			-- 		skip_unbalanced = true,
			-- 		-- better deal with markdown code blocks
			-- 		markdown = true,
			-- 	},
			-- },
		},
		event = { "InsertEnter", "CmdlineEnter" },
		opts = {
			fuzzy = { prebuilt_binaries = { download = false } },
			signature = { enabled = true, window = { border = "rounded" } }, -- Add a helper popup when writing functions, remiding the arguments
			sources = {
				default = { "dap", "dadbod", "ecolog", "snippets", "lazydev", "lsp", "buffer", "ripgrep", "path" }, -- Insert mode completions
				providers = { -- score_offset establishes a priority between sources (does not work very well though)
					dadbod = { name = "Dadbod", module = "vim_dadbod_completion.blink", score_offset = 200 },
					lazydev = { name = "lazyDev", module = "lazydev.integrations.blink", score_offset = 200 },
					snippets = { name = "snippets", module = "blink.cmp.sources.snippets", score_offset = 100 },
					cmdline = { name = "cmdline", module = "blink.cmp.sources.cmdline", score_offset = 150 },
					dap = { name = "dap", module = "blink.compat.source", score_offset = 100 },
					lsp = { name = "lsp", module = "blink.cmp.sources.lsp", score_offset = 100 },
					path = { name = "path", module = "blink.cmp.sources.path", score_offset = 100 },
					ripgrep = { name = "Ripgrep", module = "blink-ripgrep" },
					buffer = { name = "buffer", module = "blink.cmp.sources.buffer" },
					ecolog = { name = "ecolog", module = "ecolog.integrations.cmp.blink_cmp" },
				},
			},
			cmdline = {
				sources = function()
					local type = fn.getcmdtype()
					if type == "/" or type == "?" then
						return { "buffer" }
					elseif type == ":" then
						return { "cmdline", "buffer" }
					elseif type == ":lua" then
						return { "lazydev", "lsp", "buffer" }
					end
					return {}
				end,
				keymap = {
					["<CR>"] = {
						function()
							require("blink.cmp").accept({
								callback = function()
									api.nvim_feedkeys("\n", "n", true)
								end,
							})
						end,
						"accept",
						"fallback",
					},
					["<Tab>"] = { "select_next", "fallback" },
					["<S-Tab>"] = { "select_prev", "fallback" },
				},
			},
			keymap = {
				["<C-Space>"] = { "show", "show_documentation", "hide_documentation" },
				["<C-e>"] = { "hide" },
				["<CR>"] = { "accept", "fallback" },
				["<Tab>"] = { "select_next", "fallback" },
				["<S-Tab>"] = { "select_prev", "fallback" },
				["<C-d>"] = { "scroll_documentation_down", "fallback" },
				["<C-u>"] = { "scroll_documentation_up", "fallback" },
				["<C-n>"] = { "snippet_forward", "fallback" },
				["<C-p>"] = { "snippet_backward", "fallback" },
				["<Down>"] = {},
				["<Up>"] = {},
			},
			appearance = { nerd_font_variant = "mono" },
			completion = {
				list = { selection = { preselect = false, auto_insert = true } }, -- Add completion text on select
				accept = { auto_brackets = { enabled = true } }, -- Add brackets to completions
				documentation = { auto_show = true, auto_show_delay_ms = 0, window = { border = "rounded" } },
				ghost_text = { enabled = false }, -- Do not preview completions
				menu = {
					border = "rounded",
					draw = {
						treesitter = { "lsp" }, -- Highlight the "kind" icons by treesitter
						columns = { -- Nvim-cmp like layout
							{ "kind_icon", gap = 1 },
							{ "label", "label_description", gap = 1 },
						},
						components = {
							label = { -- Highlight completions with colorful-menu
								text = function(ctx)
									return require("colorful-menu").blink_components_text(ctx)
								end,
								highlight = function(ctx)
									return require("colorful-menu").blink_components_highlight(ctx)
								end,
							},
							kind_icon = { -- customize the drawing of kind icons for nvim-highlight-colors and set icons to mini.icons
								text = function(ctx)
									-- default kind icon for mini.icons
									local icon, _, _ = require("mini.icons").get("lsp", ctx.kind)
									-- if LSP source, check for color derived from documentation
									if ctx.item.source_name == "LSP" then
										local color_item = require("nvim-highlight-colors").format(
											ctx.item.documentation,
											{ kind = ctx.kind }
										)
										if color_item and color_item.abbr then
											icon = color_item.abbr
										end
									end
									return icon .. ctx.icon_gap
								end,
								highlight = function(ctx)
									-- default highlight group
									local highlight = "BlinkCmpKind" .. ctx.kind
									-- if LSP source, check for color derived from documentation
									if ctx.item.source_name == "LSP" then
										local color_item = require("nvim-highlight-colors").format(
											ctx.item.documentation,
											{ kind = ctx.kind }
										)
										if color_item and color_item.abbr_hl_group then
											highlight = color_item.abbr_hl_group
										end
									end
									return highlight
								end,
							},
						},
					},
				},
			},
		},
	},

	---------------------------
	--* Single line plugins *--
	---------------------------

	{ "OXY2DEV/helpview.nvim", ft = "help" }, --* Beautiful help pages *--
	{ "OXY2DEV/markview.nvim", opts = true }, --* Beautiful markdown previewer *--
	{ "andymass/vim-matchup", event = "VeryLazy" }, --* Better '%' action and pair matching in general *--
	{ "MagicDuck/grug-far.nvim", cmd = "GrugFar", opts = true }, --* Search & Replace *--
	{ "mvllow/modes.nvim", event = "ModeChanged", opts = true }, --* Colors cursorline based of mode *--
	{ "echasnovski/mini.diff", event = "VeryLazy", opts = true }, --* In buffer diff menu + signcolumn git info *--
	{ "folke/ts-comments.nvim", event = "VeryLazy", opts = true }, --* Change comment according to file context *--
	{ "stevearc/quicker.nvim", event = "FileType qf", opts = true }, --* Better quickfix window *--
	{ "iamyoki/buffer-reopen.nvim", keys = "<C-S-t>", opts = true }, --* Reopen close buffers (like you would do in a browser) *--
	{ "NMAC427/guess-indent.nvim", event = "VeryLazy", opts = true }, --* adapt indentation style based on the buffer *--
	{ "saecki/crates.nvim", event = "BufRead Cargo.toml", opts = true }, --* Improves crates.io management *--
	{ "zbirenbaum/neodim", event = "LspAttach", opts = { alpha = 0.5 } }, --* Dim unused code (parameters, variables, functions ...) *--
	{ "chrisgrieser/nvim-lsp-endhints", event = "LspAttach", opts = true }, --* Lsp inlay hints at the end of the line *--
	{ "echasnovski/mini-git", cmd = "Git", main = "mini.git", opts = true }, --* Git helper *--
	{ "wurli/visimatch.nvim", event = "ModeChanged", opts = { hl_group = "Folded" } }, --* highlight selected matching text *--
	{ "folke/which-key.nvim", event = "VeryLazy", opts = { win = { border = "rounded" } } }, --* Popup to remember keymaps *--
	{ "nacro90/numb.nvim", event = "CmdlineEnter", opts = { hide_relativenumbers = false } }, --* Preview line jumping in the cmdline *--
	{ "dgagn/diagflow.nvim", event = "LspAttach", opts = { scope = "line", padding_right = 1 } }, --* No visual clutter diagnostics (popup in the top right)*--
	{ "windwp/nvim-ts-autotag", ft = "html", opts = { opts = { enable_close_on_slash = true } } }, --* Auto close / rename html tags *--
	{ "echasnovski/mini.splitjoin", keys = "<leader>s", opts = { mappings = { toggle = "<leader>s" } } }, --* Split & Join arguments *--

	{ --* Beautiful csv viewver *--
		"hat0uma/csvview.nvim",
		ft = "csv",
		config = function()
			require("csvview").setup({ view = { display_mode = "border" } })
			vim.cmd("CsvViewEnable")
		end,
	},

	{ --* UI to interact with databases *--
		"kristijanhusak/vim-dadbod-ui",
		cmd = { "DBUI", "DBUIToggle", "DBUIAddConnection", "DBUIFindBuffer" },
		dependencies = {
			{ "tpope/vim-dadbod", lazy = true },
			{ "kristijanhusak/vim-dadbod-completion", ft = { "sql", "mysql", "plsql" }, lazy = true },
		},
		init = function()
			vim.g.db_ui_use_nerd_fonts = 1
		end,
	},

	{ --* extend and create a/i textobjects *--
		"echasnovski/mini.ai",
		event = "VeryLazy",
		config = function()
			local ai = require("mini.ai")

			-- register all text objects with which-key
			---@param opts table
			local function ai_whichkey(opts)
				local objects = {
					{ " ", desc = "whitespace" },
					{ '"', desc = '" string' },
					{ "'", desc = "' string" },
					{ "(", desc = "() block" },
					{ ")", desc = "() block with ws" },
					{ "<", desc = "<> block" },
					{ ">", desc = "<> block with ws" },
					{ "?", desc = "user prompt" },
					{ "U", desc = "use/call without dot" },
					{ "[", desc = "[] block" },
					{ "]", desc = "[] block with ws" },
					{ "_", desc = "underscore" },
					{ "`", desc = "` string" },
					{ "a", desc = "argument" },
					{ "b", desc = ")]} block" },
					{ "c", desc = "class" },
					{ "d", desc = "digit(s)" },
					{ "e", desc = "CamelCase / snake_case" },
					{ "f", desc = "function" },
					{ "g", desc = "entire file" },
					{ "i", desc = "indent" },
					{ "o", desc = "block, conditional, loop" },
					{ "q", desc = "quote `\"'" },
					{ "t", desc = "tag" },
					{ "u", desc = "use/call" },
					{ "{", desc = "{} block" },
					{ "}", desc = "{} with ws" },
				}

				---@type wk.Spec[]
				local ret = { mode = { "o", "x" } }
				---@type table<string, string>
				local mappings = vim.tbl_extend("force", {}, {
					around = "a",
					inside = "i",
					around_next = "an",
					inside_next = "in",
					around_last = "al",
					inside_last = "il",
				}, opts.mappings or {})
				mappings.goto_left = nil
				mappings.goto_right = nil

				for name, prefix in pairs(mappings) do
					name = name:gsub("^around_", ""):gsub("^inside_", "")
					ret[#ret + 1] = { prefix, group = name }
					for _, obj in ipairs(objects) do
						local desc = obj.desc
						if prefix:sub(1, 1) == "i" then
							desc = desc:gsub(" with ws", "")
						end
						ret[#ret + 1] = { prefix .. obj[1], desc = obj.desc }
					end
				end
				require("which-key").add(ret, { notify = false })
			end

			local config = {
				n_lines = 500,
				custom_textobjects = {
					o = ai.gen_spec.treesitter({ -- code block
						a = { "@block.outer", "@conditional.outer", "@loop.outer" },
						i = { "@block.inner", "@conditional.inner", "@loop.inner" },
					}),
					f = ai.gen_spec.treesitter({ a = "@function.outer", i = "@function.inner" }), -- function
					c = ai.gen_spec.treesitter({ a = "@class.outer", i = "@class.inner" }), -- class
					t = { "<([%p%w]-)%f[^<%w][^<>]->.-</%1>", "^<.->().*()</[^/]->$" }, -- tags
					d = { "%f[%d]%d+" }, -- digits
					e = { -- Word with case
						{
							"%u[%l%d]+%f[^%l%d]",
							"%f[%S][%l%d]+%f[^%l%d]",
							"%f[%P][%l%d]+%f[^%l%d]",
							"^[%l%d]+%f[^%l%d]",
						},
						"^().*()$",
					},
					u = ai.gen_spec.function_call(), -- u for "Usage"
					U = ai.gen_spec.function_call({ name_pattern = "[%w_]" }), -- without dot in function name
				},
			}

			ai.setup(config) -- Start the plugin
			ai_whichkey(config) -- Load which-key items
		end,
	},

	{ --* Quickly insert debug statements *--
		"andrewferrier/debugprint.nvim",
		keys = "<leader>p",
		opts = {
			keymaps = {
				normal = {
					plain_below = "<leader>p",
					plain_above = "<leader>p",
					variable_below = "<leader>pA",
					variable_above = "<leader>pa",
					variable_below_alwaysprompt = "",
					variable_above_alwaysprompt = "",
					textobj_below = "<leader>po",
					textobj_above = "<leader>pO",
					toggle_comment_debug_prints = "<leader>P",
					delete_debug_prints = "<leader>pd",
				},
				visual = {
					variable_below = "<leader>pV",
					variable_above = "<leader>pv",
				},
			},
			commands = {
				toggle_comment_debug_prints = "ToggleCommentDebugPrints",
				delete_debug_prints = "DeleteDebugPrints",
				reset_debug_prints_counter = "ResetDebugPrintsCounter",
			},
		},
	},

	{ --* Mini.nvim like QOL plugins *--
		"folke/snacks.nvim",
		priority = 1000, -- Need to start before everything
		lazy = false,
		opts = {
			indent = { enabled = true, scope = { animate = { enabled = false } } }, -- Indent lines
			input = { enabled = true }, -- Better vim.ui.input
			image = { enabled = true }, -- Show images and Latex in the terminal via the kitty graphics protocol
			words = { enabled = false }, -- Highlight LSP references under the cursor
			bigfile = { enabled = true }, -- Makes loading files > 1.5MB easier
			notifier = { enabled = false }, -- Pretty vim.notify notifications
			quickfile = { enabled = true }, -- Iender the file as quickly as possible
			statuscolumn = { enabled = false }, -- Improved statuscolumn
			dashboard = { -- Pretty dashboard
				preset = {
					pick = "fzf-lua",
          -- stylua: ignore
          keys = {
            { icon = " ", key = "f", desc = "Find File", action = ":lua Snacks.dashboard.pick('files')" },
            { icon = " ", key = "n", desc = "New File", action = ":ene | startinsert" },
            { icon = " ", key = "g", desc = "Find Text", action = ":lua Snacks.dashboard.pick('live_grep')" },
            { icon = " ", key = "o", desc = "Old Files", action = ":lua Snacks.dashboard.pick('oldfiles')" },
            { icon = " ", key = "c", desc = "Config", action = ":e $MYVIMRC" },
            { icon = " ", key = "s", desc = "Restore Session", section = "session" },
            { icon = "󰒲 ", key = "L", desc = "Lazy", action = ":Lazy", enabled = package.loaded.lazy ~= nil },
            { icon = " ", key = "q", desc = "Quit", action = ":qa!" },
          },
				},
			},
		},
    -- stylua: ignore
		keys = {
			{ "<leader>,", function() Snacks.scratch() end, desc = "Toggle Scratch Buffer" },
			{ "<leader>R", function() Snacks.rename() end, desc = "Rename File" },
			{ "<leader>g", function() Snacks.git.blame_line() end, desc = "Git blame" },
			{ "<leader>x", function() Snacks.notifier.hide() end, desc = "Dismiss All Notifications" },
		},
		init = function()
			autocmd("User", {
				pattern = "VeryLazy",
				callback = function()
					Snacks.toggle.diagnostics():map("<leader>ud")
					Snacks.toggle.inlay_hints():map("<leader>uh")
					Snacks.toggle
						.option("background", { off = "light", on = "dark", name = "Dark Background" })
						:map("<leader>ub")
				end,
			})
		end,
	},

	{ --* Highlight colors and add color completions *--
		"brenoprata10/nvim-highlight-colors",
		event = "VeryLazy",
		opts = {
			virtual_symbol_suffix = " ",
			virtual_symbol_prefix = " ",
			virtual_symbol_position = "eow",
			virtual_symbol = "󱓻",
			render = "virtual",
			enable_tailwind = true,
		},
	},

	{ --* toggle booleans, increment date & months, ... via <C-a> & <C-x> *--
		"nat-418/boole.nvim",
		keys = { "<C-a>", "<C-x>" },
		opts = { mappings = { increment = "<C-a>", decrement = "<C-x>" } },
	},

	{ --* Makes hover window prettier *--
		"Fildo7525/pretty_hover",
		opts = true,
		keys = { {
			"K",
			function()
				require("pretty_hover").hover()
			end,
			desc = "Hover",
		} },
	},

	{ --* improve & lazy load lua_ls components *--
		"folke/lazydev.nvim",
		dependencies = "Bilal2453/luvit-meta", -- Support libuv completions
		ft = "lua",
		opts = {
			library = { -- Support for external libraries
				"~/.local/share/LLS-Addons/addons/cc-tweaked", -- Support for ComputerCraft
				{ path = "snacks.nvim", words = { "Snacks" } }, -- Snacks plugin completions
				{ path = "luvit-meta/library", words = { "vim%.uv" } }, -- Support for libuv lua bindings
			},
		},
	},

	{ --* smarter splits *--
		"mrjones2014/smart-splits.nvim",
		event = "WinLeave",
		config = function()
			local sp = require("smart-splits")

			if settings.BindJKLM then
				sp.setup({ resize_mode = { resize_keys = { "j", "k", "l", "m" } } })

				-- resizing splits
				kmap("n", "<C-S-j>", sp.resize_left)
				kmap("n", "<C-S-k>", sp.resize_down)
				kmap("n", "<C-S-l>", sp.resize_up)
				kmap("n", "<C-S-m>", sp.resize_right)

				-- moving between splits
				kmap("n", "<C-j>", sp.move_cursor_left)
				kmap("n", "<C-k>", sp.move_cursor_down)
				kmap("n", "<C-l>", sp.move_cursor_up)
				kmap("n", "<C-m>", sp.move_cursor_right)

				cmd_kmap("<C-A-j>", "wincmd H", "Move window to a certain side")
				cmd_kmap("<C-A-k>", "wincmd J", "Move window to a certain side")
				cmd_kmap("<C-A-l>", "wincmd K", "Move window to a certain side")
				cmd_kmap("<C-A-m>", "wincmd L", "Move window to a certain side")

				-- Swappings windows
				kmap("n", "<C-S-A-j>", sp.swap_buf_left)
				kmap("n", "<C-S-A-k>", sp.swap_buf_down)
				kmap("n", "<C-S-A-l>", sp.swap_buf_up)
				kmap("n", "<C-S-A-m>", sp.swap_buf_right)
			else
				sp.setup({ resize_mode = { resize_keys = { "h", "j", "k", "l" } } })

				-- resizing splits
				kmap("n", "<C-S-h>", sp.resize_left)
				kmap("n", "<C-S-j>", sp.resize_down)
				kmap("n", "<C-S-k>", sp.resize_up)
				kmap("n", "<C-S-l>", sp.resize_right)

				-- moving between splits
				kmap("n", "<C-h>", sp.move_cursor_left)
				kmap("n", "<C-j>", sp.move_cursor_down)
				kmap("n", "<C-k>", sp.move_cursor_up)
				kmap("n", "<C-l>", sp.move_cursor_right)

				cmd_kmap("<C-A-h>", "wincmd H", "Move window to a certain side")
				cmd_kmap("<C-A-j>", "wincmd J", "Move window to a certain side")
				cmd_kmap("<C-A-k>", "wincmd K", "Move window to a certain side")
				cmd_kmap("<C-A-l>", "wincmd L", "Move window to a certain side")

				-- Swappings windows
				kmap("n", "<C-S-A-h>", sp.swap_buf_left)
				kmap("n", "<C-S-A-j>", sp.swap_buf_down)
				kmap("n", "<C-S-A-k>", sp.swap_buf_up)
				kmap("n", "<C-S-A-l>", sp.swap_buf_right)
			end
		end,
	},

	{ --* embed LSPs inside other filetypes (eg. in markdown code blocks) *--
		"jmbuhr/otter.nvim",
		ft = "markdown",
		dependencies = "nvim-treesitter/nvim-treesitter",
		opts = { lsp = { diagnostic_update_events = { "TextChanged" } } },
		config = function()
			require("otter").activate(languages, completion, false, tsquery) -- false: disable diagnostics
		end,
	},

	{ --* Like Telescope but by the creator of fzf (it's much faster)
		"ibhagwan/fzf-lua",
		cmd = "FzfLua",
		config = function()
			require("fzf-lua").register_ui_select() -- Replace vim.ui.select by fzf-lua
			require("fzf-lua").setup({
				fzf_bin = "sk", -- Use skim instead of fzf because it's faster
				winopts = { border = "rounded" }, -- Matches your border style
				fzf_opts = {
					["--color"] = table.concat({
						"fg:#A0A8CD",
						"fg+:#D0D8F0", -- Even brighter foreground for current line
						"hl:#7dcfff",
						"bg+:#212234",
						"hl+:#7dcfff",
						"info:#7aa2f7",
						"prompt:#7dcfff",
						"pointer:#7dcfff",
						"marker:#9ece6a",
						"spinner:#9ece6a",
						"header:#9ece6a",
						"border:#6c7086",
					}, ","),
				},
			})
		end,
    -- stylua: ignore
		keys = {
			{ "<leader>c", "<cmd>lua vim.lsp.buf.code_action()<cr>", { desc = "Code actions" } },
			{ "<leader>e", function() MiniVisits.select_path() end, desc = "Pick between most opened files" },
		},
	},

	{ --* Mappings to interact with surrounding text *--
		"echasnovski/mini.surround",
		keys = ";",
		opts = {
			mappings = {
				add = ";a", -- Add surrounding in Normal and Visual modes
				delete = ";d", -- Delete surrounding
				find = ";f", -- Find surrounding (to the right)
				find_left = ";F", -- Find surrounding (to the left)
				highlight = ";h", -- Highlight surrounding
				replace = ";r", -- Replace surrounding
				update_n_lines = ";n", -- Update `n_lines`
			},
		},
	},

	{ --* move any selection in any direction *--
		"echasnovski/mini.move",
		event = "ModeChanged",
		opts = {
			mappings = {
				left = "J",
				right = "M",
				down = "K",
				up = "L",

				line_left = "",
				line_right = "",
				line_down = "",
				line_up = "",
			},
		},
	},

	{ --* Debugger integration *--
		"mfussenegger/nvim-dap",
		dependencies = {
			{ "KingMichaelPark/mason.nvim" }, -- Needed to load DAPs
			{ "rcarriga/cmp-dap" }, -- Extra debugger information inside completions
			{ "nvim-neotest/nvim-nio" }, -- Needed IO library
			{ "rcarriga/nvim-dap-ui", opts = { floating = { border = "rounded" } } }, -- Nice UI to interact with
			{ "theHamsta/nvim-dap-virtual-text", opts = true }, -- Add extra info as virtual text
			{ "ibhagwan/fzf-lua" }, -- Used to choose things with vim.ui.select
		},
    -- stylua: ignore
    keys = {
      -- Dap
      { "<A-d>", function() require("dap").toggle_breakpoint() end, desc = "Dap | Toggle breakpoint" },
      { "<A-c>", function() require("dap").continue() end,          desc = "Dap | Continue", },
      { "<A-o>", function() require("dap").step_out() end,          desc = "Dap | Step Out", },
      { "<A-v>", function() require("dap").step_over() end,         desc = "Dap | Step Over", },

      -- DapUI
      ---@diagnostic disable-next-line: missing-parameter
      { "<A-t>", function() require("dapui").float_element() end,   desc = "DapUI | Float element", },
      ---@diagnostic disable-next-line: missing-parameter
      { "<A-K>", function() require("dapui").eval() end,            desc = "DapUI | Evaluate expression", },
      { "<A-a>", function() require("dapui").toggle() end,          desc = "DapUI | Toggle UI", },
    },
		config = function()
			local dap, dapui = require("dap"), require("dapui")

			--* C debugging with gdb and vscode-cpptools *--
			dap.adapters.cppdbg = {
				id = "cppdbg",
				type = "executable",
				command = fn.stdpath("data") .. "/mason/packages/cpptools/extension/debugAdapters/bin/OpenDebugAD7",
			}

			dap.configurations.c = {
				{
					name = "Launch file",
					type = "cppdbg",
					request = "launch",
					cwd = "${workspaceFolder}",
					stopAtEntry = true,
					program = function()
						return fn.input("Path to executable: ", fn.getcwd() .. "/", "file")
					end,
				},
				{
					name = "Attach to gdbserver :1234",
					type = "cppdbg",
					request = "launch",
					MIMode = "gdb",
					miDebuggerServerAddress = "localhost:1234",
					miDebuggerPath = "gdb",
					cwd = "${workspaceFolder}",
					program = function()
						return fn.input("Path to executable: ", fn.getcwd() .. "/", "file")
					end,
				},
			}

			--* Python debugging with debugpy *--
			dap.adapters.python = function(cb, config)
				if config.request == "attach" then
					cb({
						type = "server",
						port = assert(
							(config.connect or config).port,
							"`connect.port` is required for a python `attach` configuration"
						),
						host = (config.connect or config).host or "127.0.0.1",
						options = { source_filetype = "python" },
					})
				else
					cb({
						type = "executable",
						command = fn.stdpath("data") .. "/mason/packages/debugpy/venv/bin/python",
						args = { "-m", "debugpy.adapter" },
						options = { source_filetype = "python" },
					})
				end
			end

			dap.configurations.python = {
				{ -- The first three options are required by nvim-dap
					type = "python", -- the type here established the link to the adapter definition: `dap.adapters.python`
					request = "launch",
					name = "Launch file",

					-- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options
					program = "${file}", -- This configuration will launch the current file if used.
					pythonPath = function()
						-- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
						-- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
						-- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
						local cwd = fn.getcwd()
						if fn.executable(cwd .. "/venv/bin/python") == 1 then
							return cwd .. "/venv/bin/python"
						elseif fn.executable(cwd .. "/.venv/bin/python") == 1 then
							return cwd .. "/.venv/bin/python"
						else
							return "/usr/bin/python"
						end
					end,
				},
			}

			dap.listeners.after.event_initialized["dapui_config"] = dapui.open
			dap.listeners.before.event_terminated["dapui_config"] = dapui.close
			dap.listeners.before.event_exited["dapui_config"] = dapui.close

			fn.sign_define("DapBreakpoint", { text = "", texthl = "", linehl = "", numhl = "" }) -- nicer icons & highlight line number
			fn.sign_define("DapStopped", { text = "", texthl = "", linehl = "", numhl = "" })
		end,
	},

	{ --* Pretty commandline *--
		"folke/noice.nvim",
		event = { "CmdlineEnter", "InsertEnter" },
		dependencies = "MunifTanjim/nui.nvim", -- Needed UI lib
		opts = {
			lsp = {
				progress = { enabled = false }, -- it also shows the progress bars for diagnostics which is annoying
				override = { -- override markdown rendering so that cmp and other plugins use Treesitter
					["vim.lsp.util.convert_input_to_markdown_lines"] = true,
					["vim.lsp.util.stylize_markdown"] = true,
					["cmp.entry.get_documentation"] = false,
				},
			},
			presets = {
				bottom_search = true, -- use a classic bottom cmdline for search
				command_palette = true, -- position the cmdline and popupmenu together
				long_message_to_split = true, -- long messages will be sent to a split
				lsp_doc_border = false, -- add a border to hover docs and signature help
			},
		},
	},

	{ --* edit your filesystem like you edit a buffer *--
		"echasnovski/mini.files",
		-- event = "VeryLazy", -- Do not lazy-load if replacing netrw
		opts = {
			window = { preview = true },
			mappings = { -- TODO: add bindings for HJKL
				go_in = "m",
				go_in_plus = "M",
				go_out = "j",
				go_out_plus = "J",
				mark_set = "",
			},
		},
		init = function()
			autocmd("User", {
				desc = "mini.files rounded borders",
				pattern = "MiniFilesWindowOpen",
				callback = function(args)
					api.nvim_win_set_config(args.data.win_id, { border = "rounded" })
				end,
			})

			autocmd("User", {
				desc = "Add padding to mini.files window titles",
				pattern = "MiniFilesWindowUpdate",
				callback = function(args)
					local config = api.nvim_win_get_config(args.data.win_id)

					if config.title[#config.title][1] ~= " " then
						table.insert(config.title, { " ", "NormalFloat" })
					end
					if config.title[1][1] ~= " " then
						table.insert(config.title, 1, { " ", "NormalFloat" })
					end

					api.nvim_win_set_config(args.data.win_id, config)
				end,
			})

			autocmd("User", {
				desc = "LSP-integrated file renaming",
				pattern = "MiniFilesActionRename",
				callback = function(event)
					Snacks.rename.on_rename_file(event.data.from, event.data.to)
				end,
			})
		end,
	},

	{ --* Open links in browser *--
		"TwIStOy/gx.nvim",
		keys = { { "gx", "<cmd>Browse<cr>", mode = { "n", "x" }, desc = "Open link in browser" } }, -- Open link on cursor
		dependencies = "ibhagwan/fzf-lua",
		submodules = false, -- those are only needed for tests for the developpers
		opts = { handler_options = { search_engine = "https://search.brave.com/search?q=" } },
	},

	{ --* Nice looking icons *--
		"echasnovski/mini.icons",
		event = "VeryLazy",
		config = function()
			require("mini.icons").setup({
				lsp = { -- Icons stollen from lspkind.nvim
					text = { glyph = "󰉿" },
					method = { glyph = "󰆧" },
					["function"] = { glyph = "󰊕" },
					constructor = { glyph = "" },
					field = { glyph = "󰜢" },
					variable = { glyph = "󰀫" },
					class = { glyph = "󰠱" },
					interface = { glyph = "" },
					module = { glyph = "" },
					property = { glyph = "󰜢" },
					unit = { glyph = "󰑭" },
					value = { glyph = "󰎠" },
					enum = { glyph = "" },
					keyword = { glyph = "󰌋" },
					snippet = { glyph = "" },
					color = { glyph = "󰏘" },
					file = { glyph = "󰈙" },
					reference = { glyph = "󰈇" },
					folder = { glyph = "󰉋" },
					enumMember = { glyph = "" },
					constant = { glyph = "󰏿" },
					struct = { glyph = "󰙅" },
					event = { glyph = "" },
					operator = { glyph = "󰆕" },
					typeParameter = { glyph = "" },
				},
			})
			MiniIcons.mock_nvim_web_devicons() -- nvim_devicons compatibility layer
		end,
	},

	{ --* Adds the ability to use real formatters (not an LSP) *--
		"stevearc/conform.nvim",
		dependencies = "KingMichaelPark/mason.nvim",
		keys = {
			{
				"<leader>f",
				function()
					require("conform").format({ async = true, lsp_format = "fallback" })
				end,
				desc = "Format buffer",
			},
		},
		config = function()
			require("conform").setup({
				notify_on_error = false, -- Disable notifications for errors
				formatters_by_ft = { -- Specify formatters by file types
					go = { "gofumpt" },
					lua = { "stylua" },
					sh = { "beautysh" },
					bash = { "beautysh" },
					zsh = { "beautysh" },
					json = { "biome" },
					jsonc = { "biome" },
					javascript = { "biome" },
					c = { "clang-format" },
					cpp = { "clang-format" },
					markdown = { "mdformat", "injected" },
					python = { "ruff_format", "ruff_fix", "ruff_organize_imports" },
				},
			})
			require("conform").formatters.injected = {
				-- Injects treesitter formatting inside another filetype (eg. python code inside a markdown file)
				options = {
					ignore_errors = true,
					-- Map of treesitter language to file extension
					-- A temporary file name with this extension will be generated during formatting
					-- because some formatters care about the filename.
					lang_to_ext = {
						bash = "sh",
						c = "c",
						css = "css",
						cpp = "cpp",
						c_sharp = "cs",
						elixir = "exs",
						javascript = "js",
						julia = "jl",
						latex = "tex",
						markdown = "md",
						python = "py",
						ruby = "rb",
						rust = "rs",
						teal = "tl",
						typescript = "ts",
						asm = "asm",
						json = "json",
						toml = "toml",
						yaml = "yml",
						xml = "xml",
						lua = "lua",
						html = "html",
						vim = "vim",
					},
				},
			}
		end,
	},

	{ --* Buffer/Tab line *--
		"romgrk/barbar.nvim",
		event = "BufAdd",
		dependencies = "echasnovski/mini.icons", -- Icon support
		opts = {
			animation = false, -- Disable animations
			exclude_ft = {}, -- show every tab
			exclude_name = {}, -- show every tab
			minimum_padding = 1, -- Add a small padding between tabs
			maximum_padding = 1,
			tabpages = false, -- Do not show how many tabs are open
			clickable = false, -- Disable the clickability of tabs
			icons = {
				button = "", -- Disable close button
				modified = { button = "" }, -- Disable modified button
				separator = { left = "", right = "" }, -- do not use separators (colors do the trick)
				separator_at_end = false, -- do not use any separators
				diagnostics = { -- Do not change the tab color if there's diagnostics in the tab
					[vim.diagnostic.severity.ERROR] = { enabled = false },
					[vim.diagnostic.severity.WARN] = { enabled = false },
					[vim.diagnostic.severity.INFO] = { enabled = false },
					[vim.diagnostic.severity.HINT] = { enabled = false },
				},
			},
		},
	},

	{ --* 🎈 Floating statuslines *--
		"b0o/incline.nvim",
		event = "VeryLazy",
		dependencies = "echasnovski/mini.icons", -- icon support
		config = function()
			local loaded_buffers = false
			local ft_icon = ""
			local ft_color = ""
			local filename = ""

			autocmd("BufAdd", { -- extreme lazyloading with barbar.nvim
				callback = function()
					loaded_buffers = true
				end,
			})

			local function get_diagnostic_label(props)
				local icons = { error = " ", warn = " ", info = " ", hint = "⚑ " }
				local label = { "┊ " }
				local diagnostics = vim.diagnostic.get(props.buf) -- Cache diagnostics

				-- Get diagnostics
				local severity_counts = { error = 0, warn = 0, info = 0, hint = 0 }
				for _, diag in ipairs(diagnostics) do
					for severity, _ in pairs(icons) do
						if diag.severity == vim.diagnostic.severity[string.upper(severity)] then
							severity_counts[severity] = severity_counts[severity] + 1
						end
					end
				end

				-- Add diagnostics
				for severity, count in pairs(severity_counts) do
					if count > 0 then
						table.insert(label, { icons[severity] .. count .. " ", group = "DiagnosticSign" .. severity })
					end
				end

				if #label > 1 then -- If there's diagnostics, add a separator
					table.insert(label, { "┊ " })
				end

				return label
			end

			local function get_length(props) -- Get the length of the file
				local current_line = api.nvim_win_get_cursor(props.win)[1]
				local total_lines = api.nvim_buf_line_count(props.buf)
				return { { "  " }, { current_line }, { " / " .. total_lines .. " " } }
			end

			require("incline").setup({
				window = {
					overlap = { tabline = true }, -- Merge barbar.nvim and incline.nvim in the same line
					padding = 0,
					margin = { horizontal = 0, vertical = 0 },
				},
				render = function(props)
					local full_filename = { "" }
					if loaded_buffers == "unloaded" then
						filename = fn.fnamemodify(api.nvim_buf_get_name(props.buf), ":t")
						if filename == "" then
							filename = "[...]"
						end
						ft_icon, ft_color = require("nvim-web-devicons").get_icon_color(filename)

						full_filename = {
							{ { "┊ " } },
							{ (ft_icon or "") .. " ", guifg = ft_color, guibg = "none" },
							{ filename .. " " },
						}
					else
						full_filename = { "" }
					end

					return {
						{ get_diagnostic_label(props) },
						{ get_length(props) },
						{ full_filename },
					}
				end,
			})
		end,
	},
}, { --* Lazy.nvim opts *--
	install = { colorscheme = { "tokyonight-moon" } }, -- Load colorschemes when installing a plugin on startup
	ui = { border = "rounded" }, -- Rounded borders
	rtp = {
		disabled_plugins = { -- Disable useless vim plugins (they cause issues for other plugins)
			"gzip",
			"matchit",
			"matchparen",
			"netrwPlugin",
			"tarPlugin",
			"tohtml",
			"tutor",
			"zipPlugin",
		},
	},
})

---------------
--* Keymaps *--
---------------

--* Improved Base behaviour *--

-- terminal
kmap("t", "<Esc><Esc>", "<C-\\><C-n>", "Quit Terminal Mode") -- The desc is used to search through keymaps or to display a description for which-key.nvim
kmap("n", "<leader><", "<cmd> term <CR> i", "Open Terminal")

-- Core keymaps
cmd_kmap("<Esc>", "noh", "Remove search highlights")
kmap("n", "x", '"_x', "Do not copy a single character")
kmap({ "n", "x" }, "$", "g_", "Do not delete EOL character when using $")
kmap({ "n", "x" }, "gy", '"+y', "Copy to system clipboard")
kmap({ "n", "x" }, "gp", '"+p', "Paste from system clipboard")

-- Indent
kmap("v", ">", ">gv", "Do not go in normal mode after changing indentation")
kmap("v", "<", "<gv", "Do not go in normal mode after changing indentation")

-- Center stuff
kmap("n", "<C-u>", "<C-u>zz", "scroll up and center")
kmap("n", "<C-d>", "<C-d>zz", "scroll down and center")
kmap("n", "n", "nzzzv", "Next Search Result Centered")
kmap("n", "N", "Nzzzv", "Previous Search Result Centered")

--* Random things  *--
kmap("n", "<C-S-a>", "gg<S-v>G", "select everything")

kmap("n", "<F2>", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], "Refactor Word Under Cursor")

cmd_kmap("<C-s>", "w", "Quick save")
cmd_kmap("<C-S-s>", "w !sudo tee % >/dev/null", "Quick Sudo save")

cmd_kmap("<C-p>", "t.", "Duplicate line")

cmd_kmap("<C-b>", "lua vim.diagnostic.goto_prev({float = false})", "Goto previous diagnostic")
cmd_kmap("<C-n>", "lua vim.diagnostic.goto_next({float = false})", "Goto next diagnostic")

cmd_kmap("<F5>", "lua vim.opt.wrap=true", "Enable Line Wrap")
cmd_kmap("<F6>", "lua vim.opt.wrap=false", "Disable Line Wrap")

cmd_kmap("<A-j>", "cprev", "Backward quickfix")
cmd_kmap("<A-m>", "cnext", "Forward quickfix")

cmd_kmap("<A-k>", "lprevzz", "Backward locaction list")
cmd_kmap("<A-l>", "mnextzz", "Forward locaction list")

cmd_kmap("<leader>w", "Lazy", "Lazy")
cmd_kmap("<leader>W", "Mason", "Mason")

cmd_kmap("<leader>d", "lua MiniDiff.toggle_overlay()", "Inline git diff")
cmd_kmap("<leader>-", "lua MiniFiles.open()", "Mini.files open")

--* FzfLua keymaps *--
local fzf_kmap = function(map, cmd)
	kmap("n", "<leader>" .. map, "<cmd> FzfLua " .. cmd .. "<CR>", "FzfLua " .. cmd)
end

fzf_kmap("C", "commands")
fzf_kmap("E", "registers")
fzf_kmap("K", "keymaps")
fzf_kmap("a", "diagnostics_document")
fzf_kmap("b", "buffers")
fzf_kmap("h", "command_history")
fzf_kmap("j", "jumps")
fzf_kmap("l", "live_grep")
fzf_kmap("m", "manpages")
fzf_kmap("o", "oldfiles")
fzf_kmap("q", "quickfix")
fzf_kmap("t", "filetypes")
fzf_kmap("u", "spell_suggest")
fzf_kmap("z", "files")
fzf_kmap("²", "colorschemes")

--* Insert mode Emacs keymaps (yes it's cursed) *--
kmap("i", "<C-e>", "<C-o>$", "Goto end of the line")
kmap("i", "<C-a>", "<C-o>^", "Goto start of the line")
kmap("i", "<C-k>", "d$", "delete the end of the line")

--* Open stuff in splits *--
cmd_kmap("<leader>sv", "vsplit", "Vertical split same file")
cmd_kmap("<leader>ss", "split", "Horizontal split same file")

kmap("n", "<leader>SV", ":vsplit ", "Vertical split preset")
kmap("n", "<leader>SS", ":split ", "Horizontal split preset")

kmap("n", "<leader>SVT", ":vsplit term://", "Vertical split terminal preset")
kmap("n", "<leader>SST", ":split  term://", "Horizontal split terminal preset")

--* Run project *--
local run_kmap = function(map, cmd, desc)
	kmap("n", "<leader>sv" .. map, "<cmd>vsplit term://" .. cmd .. "<CR>i", "Vertical split" .. desc)
	kmap("n", "<leader>ss" .. map, "<cmd>vsplit term://" .. cmd .. "<CR>i", "Horizontal split" .. desc)
end

run_kmap("t", "zsh", "Open Zsh")
run_kmap("p", "python3 %", "Run Python file")
run_kmap("c", "./a.out", "Run compiled C binary")
run_kmap("g", "go run .", "Run Golang project")
run_kmap("r", "cargo run", "Run Cargo project")

--* Build based keymaps *--
local args =
	"-O2 -Wall -flto -fuse-ld=mold -march=native -mtune=native -pipe -masm=intel -Werror-implicit-function-declaration -fcf-protection -Wl,--color-diagnostics,--compress-debug-sections=zstd,--gc-sections,--pie -Werror=implicit-function-declaration -Werror=incompatible-pointer-types -Werror=return-type -fno-semantic-interposition -fpie -fstack-clash-protection -fstack-protector-strong -D_FORTIFY_SOURCE=2 -fmerge-constants -fvisibility=hidden -Wextra -Wpedantic %"
cmd_kmap("sc", "!gcc -s " .. args .. " && strip -R .comment a.out", "Build C file (optimized)")
cmd_kmap("sC", "!gcc -g -grecord-gcc-switches " .. args, "Build C file (debug)")

cmd_kmap("sg", "!go build .", "Build Golang project")
cmd_kmap("sr", "!cargo build --release")

--* File based keymaps *--
cmd_kmap("<leader>fx", '!sudo chmod +x "%"', "Make file eXecutable")
cmd_kmap("<leader>fr", '!sudo chmod +r "%"', "Make file Readable")
cmd_kmap("<leader>fw", '!sudo chmod +w "%"', "Make file Writable")

--* Keymaps inspired by browsers *--
cmd_kmap("<C-S-Tab>", "tabprevious", "Previous Tab")
cmd_kmap("<C-Tab>", "tabnext", "Next Tab")
cmd_kmap("<C-w>", "BufferClose", "Close Buffer")
cmd_kmap("<C-t>", "tabnew", "New Tab")

cmd_kmap("<C-S-W>", "tabclose", "Close Tab")
cmd_kmap("<A-S-Tab>", "BufferPrevious", "Previous Buffer")
cmd_kmap("<A-Tab>", "BufferNext", "Next Buffer")

-- Azerty
if settings.Azerty then
	cmd_kmap("<A-&>", "BufferGoto 1", "Go to buffer 1")
	cmd_kmap("<A-é>", "BufferGoto 2", "Go to buffer 2")
	cmd_kmap('<A-">', "BufferGoto 3", "Go to buffer 3")
	cmd_kmap("<A-'>", "BufferGoto 4", "Go to buffer 4")
	cmd_kmap("<A-(>", "BufferGoto 5", "Go to buffer 5")
	cmd_kmap("<A-->", "BufferGoto 6", "Go to buffer 6")
	cmd_kmap("<A-è>", "BufferGoto 7", "Go to buffer 7")
	cmd_kmap("<A-_>", "BufferGoto 8", "Go to buffer 8")
	cmd_kmap("<A-ç>", "BufferGoto 9", "Go to buffer 9")
else
	cmd_kmap("<A-1>", "BufferGoto 1", "Go to buffer 1")
	cmd_kmap("<A-2>", "BufferGoto 2", "Go to buffer 2")
	cmd_kmap("<A-3>", "BufferGoto 3", "Go to buffer 3")
	cmd_kmap("<A-4>", "BufferGoto 4", "Go to buffer 4")
	cmd_kmap("<A-5>", "BufferGoto 5", "Go to buffer 5")
	cmd_kmap("<A-6>", "BufferGoto 6", "Go to buffer 6")
	cmd_kmap("<A-7>", "BufferGoto 7", "Go to buffer 7")
	cmd_kmap("<A-8>", "BufferGoto 8", "Go to buffer 8")
	cmd_kmap("<A-9>", "BufferGoto 9", "Go to buffer 9")
end

--* Rebind hjkl to jklm *-- (Ansi Azerty french keyboard)
if settings.BindJKLM then
	kmap({ "n", "x", "v" }, "j", "h", "rebind hjkl to jklm")
	kmap({ "n", "x", "v" }, "k", "j", "rebind hjkl to jklm")
	kmap({ "n", "x", "v" }, "l", "k", "rebind hjkl to jklm")
	kmap({ "n", "x", "v" }, "m", "l", "rebind hjkl to jklm")
end

--* Neovide (a nvim GUI) settings *--
if g.neovide then
	o.mouse = "a" -- Enable mouse in neovide for scrolling
	o.linespace = -1 -- Reduce line spacing to match foot terminal
	o.guifont = "JetBrainsMono Nerd Font:h11.6:w-0.5" -- Reduce width to match foot terminal
	g.neovide_confirm_quit = false -- Do not show annoying exit popup
	g.neovide_cursor_animation_length = 0.02 -- trail time to live
	g.neovide_cursor_trail_size = 0.4 -- size of cursor trail
	g.neovide_cursor_vfx_mode = "pixiedust" -- Stardust cursor partciles
	vim.g.experimental_layer_grouping = true -- Improved blur
	vim.g.neovide_floating_corner_radius = 1.0 -- Rounded corners
end

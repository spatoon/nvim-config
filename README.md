# My Neovim Dotfile :3

So here is my nvim init.lua :

![sweet-preview](preview/sweet.png)
 
I try to make my config fast, no nosense, small, opinionated and easy to use.
Global settings can be set in `settings.json`
